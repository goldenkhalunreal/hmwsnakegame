// Copyright Epic Games, Inc. All Rights Reserved.

#include "HMWSnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HMWSnakeGame, "HMWSnakeGame" );
